<?php

namespace SeanDowney\BackpackEventsCrud\app\Http\Controllers;

use App\Models\Tag;
use SeanDowney\BackpackEventsCrud\app\Models\Event;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Setting;
use App\Models\Article;
use Illuminate\Support\Facades\DB;

class EventController extends Controller
{

    public function index()
    {
        // Get all the events
        $events = Event::with('venue')->published()->latest()->paginate();


        // Show the page
        return view('seandowney::eventscrud.index', compact('events'));
    }


    public function view($slug) {
        $recommendedArticleId = 0;
        $event = Event::with('venue')->whereSlug($slug)->published()->first();

        if (!$event) {
            abort(404, 'Please go back to our <a href="'.url('/').'">homepage</a>.');
        }

        if (isset($event->related_article)) $recommendedArticleId = $event->related_article;

        $size = 'home_muntref';
        $recommendedArticle = DB::table('articles as a')
            ->select('a.id',
                'a.archives',
                'a.articlecontent',
                'a.bajadaTitulo',
                'a.subtitulo',
                'a.title',
                'c.name as category',
                'a.galleries_id',
                'a.created_at',
                'a.image',
                'f.file as image_file',
                'f.size as image_size',
                'f.path as image_path'
            )
            ->selectRaw('CONCAT(\'mundountref/\', a.slug) AS slug')
            ->leftJoin('categories as c', 'c.id', '=', 'a.categories_id')
            ->leftJoin('attachments AS att', 'att.id', '=', 'a.image_object')
            ->leftJoin('files AS f', function ($join) use ($size) {
                $join->on('f.attachment_id', '=', 'att.id');
                $join->where('f.size', '=', DB::raw('?'));
            })
            ->leftJoin('galleries as ga', 'a.galleries_id', '=', 'ga.id')
            ->where('a.id', '=', DB::raw('?'))
            ->setBindings([$size, $recommendedArticleId])
            ->first();

        $mostRead = Article::getMostRead(4);

        // get the venue details
        if($event->venue != null){
            $event->venue = $event->venue->withFakes();
        }

        $catalogue = Setting::where('name', '=', 'Catalogo mundountref')->first();

        $tag = Tag::where('id', '=', $event->tags_id)->get();

        $this->data['event'] = $event;
        $this->data['display_ticket_form'] = $event->hasValidTickets();
        $this->data['ticket_vendors'] = config('seandowney.eventscrud.ticket_vendors');
        $this->data['recommended_article'] = $recommendedArticle;
        $this->data['most_read'] = $mostRead;
        $this->data['catalogue'] = $catalogue;
        $this->data['tags'] = $tag;

        return view('seandowney::eventscrud.event', $this->data);
    }
}
