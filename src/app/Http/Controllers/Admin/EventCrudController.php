<?php

namespace SeanDowney\BackpackEventsCrud\app\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use SeanDowney\BackpackEventsCrud\app\Http\Requests\EventRequest as StoreRequest;
use SeanDowney\BackpackEventsCrud\app\Http\Requests\EventRequest as UpdateRequest;
use Newsletter;
use App\Models\EventMailChimp;

class EventCrudController extends CrudController {

	public function setUp() {

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/
        $this->crud->setModel("SeanDowney\BackpackEventsCrud\app\Models\Event");
        $this->crud->setRoute(config('backpack.base.route_prefix').'/event');
        $this->crud->setEntityNameStrings('evento', 'eventos');
        $this->crud->setEntityNameGender('masc');


        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/

		// $this->crud->setFromDb();

		// ------ CRUD FIELDS
        $this->crud->addField([    // TEXT
            'name' => 'title',
            'label' => 'Título',
            'type' => 'text',
            'placeholder' => 'Tu título aquí',
        ]);
        $this->crud->addField([
            'name' => 'slug',
            'label' => 'Slug (URL)',
            'type' => 'text',
            'hint' => 'Será generado automaticamente si está vacio.',
        ]);
        $this->crud->addField([    // TEXT
            'name' => 'speaker',
            'label' => 'Orador',
            'type' => 'text',
            'placeholder' => 'el orador del evento',
        ]);

        $this->crud->addField([    // TEXT
            'name' => 'start_time',
            'label' => 'Inicio',
            'type' => 'datetime_picker',
            'datetime_picker_options' => [
                'format' => 'DD/MM/YYYY HH:mm',
                'language' => 'es'
            ]
        ]);

        $this->crud->addField([    // TEXT
            'name' => 'end_time',
            'label' => 'Termina',
            'type' => 'datetime_picker',
            'datetime_picker_options' => [
                'format' => 'DD/MM/YYYY HH:mm',
                'language' => 'es'
            ]
        ]);

        $this->crud->addField([    // WYSIWYG
            'name' => 'body',
            'label' => 'Contenido',
            'type' => 'ckeditor',
            'placeholder' => '',
        ]);
		$this->crud->addField([    // Image
            'name' => 'image',
            'label' => 'Imagen',
            'type' => 'browse',
        ]);

        $this->crud->addField([
            'type' => 'select2',
            'name' => 'related_article',
            'entity' => 'article',
            'attribute' => 'title',
            'model' => "App\Models\Article",
        ]);

//        $this->crud->addField([    // SELECT
//            'label' => 'Ticket Vendor',
//            'type' => 'select_from_array',
//            'name' => 'ticket_vendor',
//            'allows_null' => true,
//            'options' => config('seandowney.eventscrud.ticket_vendors'),
//            'value' => null,
//        ]);
//        $this->crud->addField([    // TEXT
//            'name' => 'ticket_vendor_id',
//            'label' => 'Ticket Vendor ID',
//            'type' => 'text',
//            'placeholder' => 'The ID from the ticket vendor',
//        ]);
		$this->crud->addField([    // SELECT
			'label' => 'Locación',
			'type' => 'select2',
			'name' => 'venue_id',
			'allows_null' => true,
			'entity' => 'venue',
			'attribute' => 'title',
			'model' => "SeanDowney\BackpackEventsCrud\app\Models\Venue",
        ]);

        $this->crud->addField([    // TEXT
            'name' => 'meta_description',
            'label' => 'Meta Description',
            'type' => 'text',
            'placeholder' => '',
        ]);

        $this->crud->addField([    // SELECT
            'label' => 'Status',
            'type' => 'select_from_array',
            'name' => 'status',
            'allows_null' => true,
            'options' => [0 => 'Draft', 1 => 'Published'],
        ]);


        $this->crud->addField([
            'name' => 'related_mailchimp_campaigns',
            'label' => 'Campañas de mailChimp relacionadas',
            'type' => 'select2_from_array',
            'options' => $this->mailChimpOptions(),
            'selected' => [], // Se completa en el metodo edit
            'allows_null' => false,
            'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
        ]);


        // ------ CRUD COLUMNS
        $this->crud->addColumns(['title', 'speaker']); // add multiple columns, at the end of the stack
        $this->crud->addColumn([
            'name' => 'start_time',
            'label' => 'Inicio',
            'type' => 'datetime',
        ]); // add a single column, at the end of the stack
        $this->crud->addColumn([
            'name' => 'status',
            'label' => 'Status',
            'type' => 'boolean',
            'options' => [0 => 'Draft', 1 => 'Published'],
        ]);

        $this->crud->addButtonFromView('line', 'event_analytics', 'event_analytics', 'end');


        $this->crud->orderBy('start_time', 'desc');
    }


    public function store(StoreRequest $request)
    {
        $this->beforeSave();

        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        $this->beforeSave();

        return parent::updateCrud();
    }

    private function beforeSave() {
        $eventId = $this->crud->request->request->get('id');
        $event = EventMailChimp::find($eventId);
        if ($event != null) $event->delete();
        $campaignsList = $this->crud->request->request->get('related_mailchimp_campaigns');
        if ($campaignsList != null) {
            foreach ($campaignsList as $c) {
                $eventMailChimp = new EventMailChimp();
                $eventMailChimp->id = $eventId;
                $eventMailChimp->mailchimp_campaign_id = $c;
                $eventMailChimp->save();
            }
        }
   }

   private function mailChimpOptions() {
       $mailChimp = Newsletter::getApi()->get('campaigns', ['fields' => 'campaigns'])['campaigns'];
       $options = array();
       foreach ($mailChimp as $m) {
           $options[$m['id']] = $m['settings']['title'];
       }

       return $options;
   }

    public function edit($id) {
        $this->crud->hasAccessOrFail('update');

        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;
        $this->data['fields'] = $this->crud->getUpdateFields($id);

        $selectedCampaigns = EventMailChimp::where('id', '=', $id)->get()->pluck('mailchimp_campaign_id')->toArray();
        $this->data['fields']['related_mailchimp_campaigns']['selected'] = $selectedCampaigns;

        $this->data['title'] = trans('backpack::crud.edit').' '.$this->crud->entity_name;
        $this->data['id'] = $id;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view('crud::edit', $this->data);
    }
}
