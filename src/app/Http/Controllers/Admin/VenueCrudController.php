<?php

namespace SeanDowney\BackpackEventsCrud\app\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use SeanDowney\BackpackEventsCrud\app\Http\Requests\VenueRequest as StoreRequest;
use SeanDowney\BackpackEventsCrud\app\Http\Requests\VenueRequest as UpdateRequest;

class VenueCrudController extends CrudController {

	public function setUp() {

        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/
        $this->crud->setModel("SeanDowney\BackpackEventsCrud\app\Models\Venue");
        $this->crud->setRoute(config('backpack.base.route_prefix').'/venue');
        $this->crud->setEntityNameStrings('locación', 'locaciones');
        $this->crud->setEntityNameGender('fem');


        /*
		|--------------------------------------------------------------------------
		| BASIC CRUD INFORMATION
		|--------------------------------------------------------------------------
		*/

		// $this->crud->setFromDb();

		// ------ CRUD FIELDS
        $this->crud->addField([    // TEXT
            'name' => 'title',
            'label' => 'Título',
            'type' => 'text',
            'placeholder' => '',
        ]);
        $this->crud->addField([
            'name' => 'slug',
            'label' => 'Slug (URL)',
            'type' => 'text',
            'hint' => 'Will be automatically generated from your title, if left empty.',
        ]);
        $this->crud->addField([    // WYSIWYG
            'name' => 'description',
            'label' => 'Descripción',
            'type' => 'ckeditor',
            'placeholder' => '',
        ]);
		$this->crud->addField([    // TEXT
            'name' => 'address1',
            'label' => 'Dirección Linea 1',
            'type' => 'text',
            'placeholder' => '',
        ]);

		$this->crud->addField([    // TEXT
            'name' => 'address2',
            'label' => 'Dirección Linea 2',
            'type' => 'text',
            'placeholder' => '',
        ]);

		$this->crud->addField([    // TEXT
            'name' => 'city',
            'label' => 'Pueblo/Ciudad',
            'type' => 'text',
            'placeholder' => '',
        ]);

		$this->crud->addField([    // TEXT
            'name' => 'state',
            'label' => 'Provincia',
            'type' => 'text',
            'placeholder' => '',
        ]);

		$this->crud->addField([    // TEXT
            'name' => 'postcode',
            'label' => 'Código postal',
            'type' => 'text',
            'placeholder' => '',
        ]);

		$this->crud->addField([    // TEXT
            'name' => 'country',
            'label' => 'País',
            'type' => 'text',
            'placeholder' => '',
        ]);

		$this->crud->addField([    // TEXT
            'name' => 'url',
            'label' => 'URL',
            'type' => 'url',
            'placeholder' => '',
        ]);

		$this->crud->addField([    // TEXT
            'name' => 'phone',
            'label' => 'Número de teléfono',
            'type' => 'text',
            'placeholder' => '',
        ]);

		$this->crud->addField([    // TEXT
            'name' => 'latitude',
            'label' => 'Latitud',
            'type' => 'text',
			'fake' => true,
            'store_in' => 'coordinates',
        ]);

		$this->crud->addField([    // TEXT
            'name' => 'longitude',
            'label' => 'Longitud',
            'type' => 'text',
			'fake' => true,
            'store_in' => 'coordinates',
        ]);

        // ------ CRUD COLUMNS
        $this->crud->addColumns(['title', 'description']); // add multiple columns, at the end of the stack

        $this->crud->orderBy('title', 'desc');
    }


	public function store(StoreRequest $request)
	{
		return parent::storeCrud();
	}

	public function update(UpdateRequest $request)
	{
		return parent::updateCrud();
	}
}
